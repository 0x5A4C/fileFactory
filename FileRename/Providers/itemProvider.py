
import os

from Common.common import CommonItem, CommonException
from Items.fileSystemItem import FileSystemItem

#------------------------------------------------------------------------------------------------------------------------------------
#--ItemProvider----------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------

class CommonItemProvider(CommonItem):
    def __init__(self):
        CommonItem.__init__(self)

    def process(self):
        raise NotImplementedError("Please Implement this method")        

class CommonItemProviderException(CommonException):
    def __init__(self, msg):
        CommonException.__init__(self, msg)
        
class ItemProvider(CommonItemProvider):
    def __init__(self):
        CommonItemProvider.__init__(self)
        
    def process(self):
        raise NotImplementedError("Please Implement this method")        

class ItemProviderException(CommonItemProviderException):
    def __init__(self, msg):
        CommonItemProviderException.__init__(self, msg)

#--FileSystemItemProvider------------------------------------------------------------------------------------------------------------

class FileSystemItemProvider(ItemProvider):
    path = None
    fileItems = []
    
    def __init__(self, path):
        ItemProvider.__init__(self)
        self.path = path
        
    def process(self):
        self.logger.info("Processing: {0}".format(self.path))
#        raise NotImplementedError("Please Implement this method")
        try:
            for root, dirs, files in os.walk(self.path, topdown=False):
                for name in files:
                    self.fileItems.append(FileSystemItem(os.path.join(root, name)))
#                    self.fileItems.append(os.path.join(root, name))
                
                self.logger.info("path: {0}".format(root))
                self.logger.info("subdirs: {0}".format(len(dirs)))
                self.logger.info("files: {0}".format(len(files)))
                
                self.logger.info("fileItems: {0}".format(len(self.fileItems)))
        except Exception as ex:
            self.logger.error("Exception: {0}".format(str(ex)))
            raise ItemProviderException("Error while processing: {0}".format(self.path))

