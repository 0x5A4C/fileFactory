
from itemTask import ItemTask, ItemTaskException
from Tasks.itemTask import ItemTaskMove

#------------------------------------------------------------------------------------------------------------------------------------
#CommonHashedItemTask
#------------------------------------------------------------------------------------------------------------------------------------
class CommonHashedItemTask(ItemTask):

    dryRun = True

    def __init__(self):
        ItemTask.__init__(self)
   
    def process(self, hashedItems):
        raise NotImplementedError("Please Implement this method")

#------------------------------------------------------------------------------------------------------------------------------------
class CommonHashedItemTaskException(ItemTaskException):    
    def __init__(self, message = ""):
        ItemTaskException.__init__(self, message)

#------------------------------------------------------------------------------------------------------------------------------------
#HashedItemTask
#------------------------------------------------------------------------------------------------------------------------------------
class HashedItemTask(CommonHashedItemTask):

    dryRun = True

    def __init__(self):
        CommonHashedItemTask.__init__(self)
   
    def process(self, hashedItems):
        self.logger.info("hashedItems: {0}".format(len(hashedItems)))
        
        for hashedItem in hashedItems:
            pass       
        raise NotImplementedError("Please Implement this method")

#------------------------------------------------------------------------------------------------------------------------------------
class HashedItemTaskException(CommonHashedItemTaskException):    
    def __init__(self, message = ""):
        CommonHashedItemTaskException.__init__(self, message)
 
 #------------------------------------------------------------------------------------------------------------------------------------
#HashedItemTask_FindDuplicates
#------------------------------------------------------------------------------------------------------------------------------------
class HashedItemTask_FindDuplicates(HashedItemTask, ItemTaskMove):

#     dryRun                  = True
    hashedItems             = {}
    hashedDuplicatedItems   = {}
    
    def __init__(self, destinationFormat, fileNameFormat = None, dryRun = True):
        HashedItemTask.__init__(self)
        ItemTaskMove.__init__(self, destinationFormat, fileNameFormat, dryRun)
   
    def process(self, hashedItems):
        self.logger.info("hashedItems: {0}".format(len(hashedItems)))
        
        for hashedItem in hashedItems:
            if hashedItem.hash in self.hashedItems:
#                self.hashedDuplicatedItems = {hashedItem.hash: hashedItem.fileSystemItem}
                self.hashedDuplicatedItems[hashedItem.hash] = hashedItem.fileSystemItem
            else:
 #               self.hashedItems = {hashedItem.hash: hashedItem.fileSystemItem}                
                self.hashedItems[hashedItem.hash] = hashedItem.fileSystemItem

        self.logger.info("hashedItems: {0}".format(len(self.hashedItems)))
        self.logger.info("hashedDuplicatedItems: {0}".format(len(self.hashedDuplicatedItems)))
        
        duplicatedFilesystemItems = []
        for hashedDuplicatedItem in self.hashedDuplicatedItems.values():
            duplicatedFilesystemItems.append(hashedDuplicatedItem)

        for duplicatedFilesystemItem in duplicatedFilesystemItems:
            self.move(duplicatedFilesystemItem)
            
#------------------------------------------------------------------------------------------------------------------------------------
class HashedItemTask_FindDuplicates_Exception(HashedItemTaskException):    
    def __init__(self, message = ""):
        HashedItemTaskException.__init__(self, message)
        