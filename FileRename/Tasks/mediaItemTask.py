# coding: utf-8

import os, shutil

from Common.common import CommonValueErrorException
from Tasks.itemTask import ItemTask, ItemTaskException, ItemTaskCopy
#------------------------------------------------------------------------------------------------------------------------------------
#MediaItemTask
#------------------------------------------------------------------------------------------------------------------------------------
class CommonMediaItemTask(ItemTask):

    dryRun = True

    def __init__(self):
        ItemTask.__init__(self)
        pass

    def copy(self, mediaItem):
        raise NotImplementedError("Please Implement this method")

    def process(self, mediaItems):
        raise NotImplementedError("Please Implement this method")

#------------------------------------------------------------------------------------------------------------------------------------
class CommonMediaItemTaskException(ItemTaskException):
    def __init__(self, message = ""):
        ItemTaskException.__init__(self, message)

#------------------------------------------------------------------------------------------------------------------------------------
class  MediaItemTaskValueErrorException(CommonValueErrorException):
    def __init__(self, message):
        CommonValueErrorException.__init__(self, message)

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemTaskException(CommonMediaItemTaskException):
    def __init__(self, message = ""):
        CommonMediaItemTaskException.__init__(self, message)

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemTask(CommonMediaItemTask):
    def __init__(self):
        CommonMediaItemTask.__init__(self)
        pass

    def process(self, mediaItems):
        raise NotImplementedError("Please Implement this method")

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemTask_Copy(MediaItemTask, ItemTaskCopy):
    destination         = ""
    destinationFormat   = ""

    def __init__(self, destinationFormat, fileNameFormat = None, dryRun = True):
        MediaItemTask.__init__(self)
        ItemTaskCopy.__init__(self, destinationFormat, fileNameFormat, dryRun)

#         self.destinationFormat = destinationFormat
#         self.fileNameFormat = fileNameFormat
#         self.dryRun = dryRun

    def copy(self, mediaItem):
#creating dest directory
        self.getDestination(mediaItem.dateTime, None)
        if(self.dryRun != True):
            try:
                if not os.path.isdir(self.destination):
                    os.makedirs(self.destination)
            except Exception, ex:
                self.logger.error("Exception: {0}".format(str(ex)))
                raise MediaItemTaskException("Error while creating {0}".format(self.destination))
#copying items
            if(mediaItem.fileSystemItem != None):
                self.getDestination(mediaItem.dateTime, mediaItem.fileSystemItem)
                try:
                    self.sysCopy(mediaItem.fileSystemItem)
                except Exception, ex:
                    self.logger.error("Exception: {0}".format(str(ex)))
#TODO: Do something with this!

                for fileSystemItem in mediaItem.fileSystemItem.subFileSysytemItems.values():
                    self.getDestination(mediaItem.dateTime, fileSystemItem)
                    try:
                        self.sysCopy(fileSystemItem)
                    except Exception, ex:
                        self.logger.error("Exception: {0}".format(str(ex)))
#TODO: Do something with this!

            else:
                raise MediaItemTaskException("fileSystemItem == None!")

    def process(self, mediaItems):
        self.logger.info("mediaItems: {0}".format(len(mediaItems)))

        for mediaItem in mediaItems:
            self.copy(mediaItem)
