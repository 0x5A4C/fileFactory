
import os, shutil
from Common.common import CommonItem, CommonException

#------------------------------------------------------------------------------------------------------------------------------------
#CommonItemTask
#------------------------------------------------------------------------------------------------------------------------------------
class CommonItemTask(CommonItem):

    dryRun = True

    def __init__(self):
        CommonItem.__init__(self)
   
    def process(self, mediaItems):
        raise NotImplementedError("Please Implement this method")

#------------------------------------------------------------------------------------------------------------------------------------
class CommonItemTaskException(CommonException):    
    def __init__(self, message = ""):
        CommonException.__init__(self, message)
        
#------------------------------------------------------------------------------------------------------------------------------------
#ItemTask
#------------------------------------------------------------------------------------------------------------------------------------
class ItemTask(CommonItemTask):

    dryRun = True

    def __init__(self):
        CommonItemTask.__init__(self)
 
    def process(self, items):
        raise NotImplementedError("Please Implement this method")

#------------------------------------------------------------------------------------------------------------------------------------
class ItemTaskException(CommonItemTaskException):    
    def __init__(self, message = ""):
        CommonItemTaskException.__init__(self, message)
 
 #------------------------------------------------------------------------------------------------------------------------------------
#ItemCommonFileTask
#------------------------------------------------------------------------------------------------------------------------------------
class ItemCommonFileTask(ItemTask):
    destination         = ""
    destinationFormat   = ""
    dryRun = True


    def __init__(self, destinationFormat, fileNameFormat = None, dryRun = True):
        ItemTask.__init__(self)
        self.destinationFormat = destinationFormat
        self.fileNameFormat = fileNameFormat
        self.dryRun = dryRun
   
    def renameDuplicate(self, destination, num=1):
        #splits file name to add number distinction
        (filePrefix, fileExt) = os.path.splitext(destination)
        renamed = "{0}({1}){2}".format(filePrefix,num,fileExt)
     
        #checks if renamed file exists. Renames file if it does exist.
        if os.path.exists(renamed):
            return self.renameDuplicate(destination, num + 1)
        else:
            return renamed

    def getDestination(self, dateTime, fileSystemItem):
        destinationFormatDict = { "YYYY":       dateTime.strftime("%Y"),
                                  "YY":         dateTime.strftime("%y"),  
                                  "MM":         dateTime.strftime("%m"),
                                  "DD":         dateTime.strftime("%d"),
                                  "ss":         dateTime.strftime("%S"),
                                  "mm":         dateTime.strftime("%M"),
                                  "hh":         dateTime.strftime("%H"),
                                  "date":       dateTime.strftime("%x"),
                                  "time":       dateTime.strftime("%X"),
                                  "datetime":   dateTime.strftime("%c"),
                                }        
        self.destination  = self.destinationFormat.format(**destinationFormatDict)
        
        if(self.fileNameFormat != None and fileSystemItem != None):
            fileNameFormatDict =    {
                                      "fileName":   fileSystemItem.name.id,
                                      "fileExt":    fileSystemItem.extension.id,
                                    }            
            self.destination  = os.path.join(self.destination, self.fileNameFormat.format(**dict(destinationFormatDict.items() + fileNameFormatDict.items())))
   
   
    def process(self, items):
        raise NotImplementedError("Please Implement this method")

#------------------------------------------------------------------------------------------------------------------------------------
class ItemCommonFileTaskException(ItemTaskException):    
    def __init__(self, message = ""):
        CommonItemTaskException.__init__(self, message)
 
#------------------------------------------------------------------------------------------------------------------------------------
#ItemTaskCopy
#------------------------------------------------------------------------------------------------------------------------------------
class ItemTaskCopy(ItemCommonFileTask):

    def __init__(self, destinationFormat, fileNameFormat = None, dryRun = True):
        ItemCommonFileTask.__init__(self, destinationFormat, fileNameFormat, dryRun)
        self.destinationFormat = destinationFormat
        self.fileNameFormat = fileNameFormat
        self.dryRun = dryRun

    def sysCopy(self, fileSystemItem, duplicateIndex=0):
        try:
            self.logger.info("Copying: {0} to: {1}".format(fileSystemItem.fullFileName, self.destination))
            if(os.path.isfile(self.destination) == True):
                self.destination = self.renameDuplicate(self.destination)
            
            shutil.copy(fileSystemItem.fullFileName, self.destination)                      
        except Exception, ex:
            self.logger.error("Exception: {0}".format(str(ex)))
            raise ItemTaskCopyException("Error while copying {0}".format(fileSystemItem.fullFileName))        
        pass

    def copy(self, fileItem):
#creating dest directory        
        self.getDestination(fileItem.dateTime, None)             
        if(self.dryRun != True):
            if(fileItem != None):
                try:
                    if not os.path.isdir(self.destination):
                        os.makedirs(self.destination)
                except Exception, ex:
                    self.logger.error("Exception: {0}".format(str(ex)))
                    raise ItemTaskCopyException("Error while creating {0}".format(self.destination))            
#copying items
                self.getDestination(fileItem.dateTime, fileItem.fileSystemItem)
                self.sysCopy(fileItem.fileSystemItem)                
            else:
                raise ItemTaskCopyException("fileSystemItem == None!")   
            
    def process(self, items):
        raise NotImplementedError("Please Implement this method")            
#------------------------------------------------------------------------------------------------------------------------------------
class ItemTaskCopyException(ItemTaskException):    
    def __init__(self, message = ""):
        ItemTaskException.__init__(self, message)
        
#------------------------------------------------------------------------------------------------------------------------------------
#ItemTaskMove
#------------------------------------------------------------------------------------------------------------------------------------
class ItemTaskMove(ItemCommonFileTask):

    def __init__(self, destinationFormat, fileNameFormat = None, dryRun = True):
        ItemCommonFileTask.__init__(self, destinationFormat, fileNameFormat, dryRun)
        self.destinationFormat = destinationFormat
        self.fileNameFormat = fileNameFormat
        self.dryRun = dryRun

    def sysMove(self, fileSystemItem, duplicateIndex=0):
        try:
            self.logger.info("Copying: {0} to: {1}".format(fileSystemItem.fullFileName, self.destination))
            if(os.path.isfile(self.destination) == True):
                self.destination = self.renameDuplicate(self.destination)
            
            shutil.move(fileSystemItem.fullFileName, self.destination)                      
        except Exception, ex:
            self.logger.error("Exception: {0}".format(str(ex)))
            raise ItemTaskCopyException("Error while moving {0}".format(fileSystemItem.fullFileName))        
        pass

    def move(self, fileItem):
#creating dest directory        
        self.getDestination(fileItem.dateTime, None)
        if(self.dryRun != True):
            if(fileItem != None):
                try:
                    if not os.path.isdir(self.destination):
                        os.makedirs(self.destination)
                except Exception, ex:
                    self.logger.error("Exception: {0}".format(str(ex)))
                    raise ItemTaskCopyException("Error while creating {0}".format(self.destination))            
#copying items
                self.getDestination(fileItem.dateTime, fileItem)
                self.sysMove(fileItem)                
            else:
                raise ItemTaskMoveException("fileSystemItem == None!")   

    def process(self, items):
        raise NotImplementedError("Please Implement this method")            
#------------------------------------------------------------------------------------------------------------------------------------
class ItemTaskMoveException(ItemTaskException):    
    def __init__(self, message = ""):
        ItemTaskException.__init__(self, message)
                