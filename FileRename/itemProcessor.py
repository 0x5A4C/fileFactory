
from Common.common import CommonItem, CommonException

#------------------------------------------------------------------------------------------------------------------------------------
#ItemProcessor
#------------------------------------------------------------------------------------------------------------------------------------

class CommonItemProcessor(CommonItem):
    def __init__(self):
        CommonItem.__init__(self)
        
class CommonItemProcessorException(CommonException):
    def __init__(self):
        CommonItem.__init__(self)
        
class ItemProcessor(CommonItemProcessor):
    def __init__(self):
        CommonItemProcessor.__init__(self)
        
class ItemProcessorException(CommonItemProcessorException):
    def __init__(self):
        CommonItemProcessorException.__init__(self)
