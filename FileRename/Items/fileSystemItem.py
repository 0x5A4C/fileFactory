
import os
import os.path
import datetime

from Common.common import CommonItem, CommonException
from Items.fileComponent import FileComponent_Extension, FileComponent_Name, FileComponent_Path, FileComponent_Exif, FileComponent_Hash

#------------------------------------------------------------------------------------------------------------------------------------
# FileSystemItem
#------------------------------------------------------------------------------------------------------------------------------------
class CommonFileSystemItem(CommonItem):    
    extension    = None
    name         = None
    path         = None
    
    dateTime     = None
    
    fullFileName = ""
    
    subFileSysytemItems = {}
    
    def __init__(self, fileName = ""):
        CommonItem.__init__(self)
        
        if not os.path.isfile(fileName):
            raise CommonFileSystemException("Provide file not exists: {0}".format(fileName))
        
        (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(fileName)
        self.dateTime = datetime.datetime.fromtimestamp(mtime)
        
        self.fullFileName = fileName
        self.logger.debug("FileSystemItem: {0}".format(self.fullFileName))
        
        self.extension = FileComponent_Extension(os.path.splitext(fileName)[1][1:].strip().lower())
        self.name = FileComponent_Name(os.path.split(fileName)[1])
        self.path = FileComponent_Path(os.path.split(fileName)[0])

        self.subFileSysytemItems = {}
#------------------------------------------------------------------------------------------------------------------------------------
class CommonFileSystemException(CommonException):    
    def __init__(self, message = ""):
        CommonException.__init__(self, message)

#------------------------------------------------------------------------------------------------------------------------------------
class FileSystemItem(CommonFileSystemItem):    
    def __init__(self, fileName=""):
        try:
            CommonFileSystemItem.__init__(self, fileName)
        except Exception as ex:
            self.logger.error("Exception: {0}".format(str(ex)))
            raise FileSystemException(str(ex))
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class FileSystemException(CommonFileSystemException):    
    def __init__(self, message = ""):
        CommonFileSystemException.__init__(self, message)
