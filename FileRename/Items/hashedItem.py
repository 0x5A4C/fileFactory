import os
import hashlib

from Common.common import CommonItem, CommonException

#------------------------------------------------------------------------------------------------------------------------------------
# HashedItem
#------------------------------------------------------------------------------------------------------------------------------------
class CommonHashedItemException(CommonException):    
    def __init__(self, message = None):
        CommonException.__init__(self, message)
        pass

class HashedItemException(CommonHashedItemException):    
    def __init__(self, message = None):
        CommonHashedItemException.__init__(self, message)
        pass

#------------------------------------------------------------------------------------------------------------------------------------    
class CommonHashedItem(CommonItem):
    fileSystemItem = None
    hash = None
    
    def __init__(self, fileSystemItem = None):
        CommonItem.__init__(self)

        if(fileSystemItem != None):
            self.fileSystemItem = fileSystemItem
            self.logger.debug("CommonHashedItem created for {0}".format(self.fileSystemItem.fullFileName))
            
            try:
                fileSystemItemBuffer = open(self.fileSystemItem.fullFileName).read()
                self.hash = hashlib.md5(fileSystemItemBuffer).hexdigest()
            except Exception, ex:
                self.logger.error("Exception: {0}".format(str(ex)))
                raise CommonHashedItemException("Error while creating hash for: {0}".format(self.fileSystemItem.fullFileName))  

#------------------------------------------------------------------------------------------------------------------------------------
class HashedItem(CommonHashedItem):

    def __init__(self, fileSystemItem = None):
        try:
            CommonHashedItem.__init__(self, fileSystemItem)            
        except CommonHashedItemException, ex:
            self.logger.error("Exception: {0}".format(str(ex)))
            raise HashedItemException("Error while creating hash for: {0}".format(self.fileSystemItem.fullFileName))  
