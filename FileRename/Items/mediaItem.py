import os
import exifread
import datetime
import subprocess

from Common.common import CommonItem, CommonException
from Items.fileSystemItem import FileSystemItem

#------------------------------------------------------------------------------------------------------------------------------------
# MediaItem
#------------------------------------------------------------------------------------------------------------------------------------
class CommonMediaItemException(CommonException):
    def __init__(self, message = None):
        CommonException.__init__(self, message)
        pass

class MediaItemException(CommonMediaItemException):
    def __init__(self, message = None):
        CommonMediaItemException.__init__(self, message)
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class CommonMediaItem(CommonItem):
    id = "Undefined"
    fileSystemItem = None
    dateTime = None

    def __init__(self, fileSystemItem = None):
        CommonItem.__init__(self)
        if(fileSystemItem != None):
            if(fileSystemItem.extension.id.lower() != self.id.lower()):
                raise CommonMediaItemException()
            else:
                self.logger.debug("CommonMediaItem created for {0}".format(self.id))
        self.fileSystemItem = fileSystemItem
        self.dateTime = self.fileSystemItem.dateTime
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemPicture(CommonMediaItem):
    exif = None
    fileCreationTimeStamp = None
    def __init__(self, fileSystemItem = None):
        CommonMediaItem.__init__(self, fileSystemItem)
        if(self.fileSystemItem != None):
            self.setDateTimeFromExif(self.fileSystemItem)

    def getExif(self, pitureFileName):
        tags = None
        pictureFile = open(pitureFileName, 'rb')
        try:
            tags = exifread.process_file(pictureFile)
        except:
            self.logger.error("Error while reading exiff for file: {0}".format(pitureFileName))
            pass
        pictureFile.close()
        return tags

    def getExifField(self, fieldId):
        if(self.exif != None):
            for (key, value) in self.exif.iteritems():
    #            fieldID = TAGS.get(k)
                if(key.find(fieldId) != -1):
                    return value

    def setDateTimeFromExif(self, fileSystemItem):
        if(fileSystemItem != None):
            self.exif = self.getExif(fileSystemItem.fullFileName)
            if(self.exif != None and self.exif != {}):
                self.logger.debug("{0}: Exif created".format(fileSystemItem.fullFileName))
                dateTimeExifKeys = ["DateTimeDigitized", "DateTime", "DateTimeOriginal", "EXIF DateTimeDigitized", "EXIF DateTime", "EXIF DateTimeOriginal"]
                for dateTimeExifKey in dateTimeExifKeys:
                    if(dateTimeExifKey in self.exif.keys()):
                        self.dateTime = datetime.datetime.strptime(str(self.getExifField(dateTimeExifKey)), '%Y:%m:%d %H:%M:%S')
                        break

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemPictureException(CommonMediaItemException):
    def __init__(self, message = ""):
        CommonMediaItemException.__init__(self, message)
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemMovie(CommonMediaItem):
    def __init__(self, fileSystemItem = None):
        CommonMediaItem.__init__(self, fileSystemItem)
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemMovieException(CommonMediaItemException):
    def __init__(self, message = ""):
        CommonMediaItemException.__init__(self, message)
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemPicture_Jpg(MediaItemPicture):
    id = "jpg"
    def __init__(self, fileSystemItem = None):
        MediaItemPicture.__init__(self, fileSystemItem)
#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemPicture_Jpeg(MediaItemPicture):
    id = "jpeg"
    def __init__(self, fileSystemItem = None):
        MediaItemPicture.__init__(self, fileSystemItem)

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemPicture_Avi(MediaItemMovie):
    id = "avi"
    def __init__(self, fileSystemItem = None):
        MediaItemMovie.__init__(self, fileSystemItem)

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemPicture_Crw(MediaItemPicture):
    id = "crw"
    def __init__(self, fileSystemItem = None):
        MediaItemPicture.__init__(self, fileSystemItem)

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemPicture_Thm(MediaItemPicture):
    id = "thm"
    def __init__(self, fileSystemItem = None):
        MediaItemPicture.__init__(self, fileSystemItem)

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemPicture_CanonRaw(MediaItemPicture):
    id = "crw"

    def __init__(self, fileSystemItem = None):
        MediaItemPicture.__init__(self, fileSystemItem)

        subFilePath = os.path.join(self.fileSystemItem.path.id, os.path.splitext(self.fileSystemItem.name.id)[0] + "." + "THM")
        if(os.path.isfile(subFilePath)):
            self.fileSystemItem.subFileSysytemItems["thm"] = FileSystemItem(subFilePath)
            self.setDateTimeFromExif(self.fileSystemItem.subFileSysytemItems["thm"])

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemPicture_CanonAvi(MediaItemPicture):
    id = "avi"

    def __init__(self, fileSystemItem = None):
        MediaItemPicture.__init__(self, fileSystemItem)

        subFilePath = os.path.join(self.fileSystemItem.path.id, os.path.splitext(self.fileSystemItem.name.id)[0] + "." + "THM")
        if(os.path.isfile(subFilePath)):
            self.fileSystemItem.subFileSysytemItems["thm"] = FileSystemItem(subFilePath)
            self.setDateTimeFromExif(self.fileSystemItem.subFileSysytemItems["thm"])

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemPicture_Mp4(MediaItemMovie):
    id = "mp4"
    def __init__(self, fileSystemItem = None):
        MediaItemMovie.__init__(self, fileSystemItem)

#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemPicture_Mov(MediaItemMovie):
    id = "mov"
    def __init__(self, fileSystemItem = None):
        MediaItemMovie.__init__(self, fileSystemItem)

#------------------------------------------------------------------------------------------------------------------------------------
