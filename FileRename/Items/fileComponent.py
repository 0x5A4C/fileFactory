
from Common.common import CommonItem, CommonException

#------------------------------------------------------------------------------------------------------------------------------------
# FileComponent
#------------------------------------------------------------------------------------------------------------------------------------
class CommonFileComponent(CommonItem):
    id = ""

    def __init__(self, idStr=""):
        CommonItem.__init__(self)
        self.id = idStr
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class FileComponent(CommonFileComponent):
    def __init__(self, idStr=""):
        CommonFileComponent.__init__(self, idStr)
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class FileComponent_Extension(FileComponent):
    def __init__(self, idStr=""):
        FileComponent.__init__(self, idStr)
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class FileComponent_Name(FileComponent):
    def __init__(self, idStr=""):
        FileComponent.__init__(self, idStr)
        pass
    
#------------------------------------------------------------------------------------------------------------------------------------
class FileComponent_Path(FileComponent):
    def __init__(self, idStr=""):
        FileComponent.__init__(self, idStr)
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class FileComponent_Exif(FileComponent):
    def __init__(self):
        FileComponent.__init__(self, "Exif")
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class FileComponent_Hash(FileComponent):
    def __init__(self):
        FileComponent.__init__(self, "Hash")
        pass
