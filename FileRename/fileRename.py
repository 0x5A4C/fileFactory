from PIL import Image
from PIL.ExifTags import TAGS
from Providers.itemProvider       import FileSystemItemProvider
from itemProcessor                import ItemProcessor
#Factories
from ItemFactory.mediaItemFactory  import MediaItemFactory
from ItemFactory.hashedItemFactory import HashedItemFactory
#Tasks
from Tasks.mediaItemTask           import MediaItemTask_Copy
from Tasks.hashedItemTask          import HashedItemTask_FindDuplicates

from os.path import join, getsize
import dbus
import logging
import logging.config
import os
import os.path
import sys
import sys
import traceback

from optparse import OptionParser
import argparse
import os
import tempfile
import shutil
import atexit

#------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------
def initGlobalLogger():

    # logging.config.fileConfig("gStarred.logging.conf")

    loggerPrevInst = logging.getLogger('')
    while loggerPrevInst.handlers:
        loggerPrevInst.handlers.pop()

    # set up default logging (console)
    logging.basicConfig(level=logging.INFO, format='%(name)-12s: %(levelname)-8s %(message)s', datefmt='%m-%d %H:%MgStarred.py')
    # set up logging to file
    handler = logging.handlers.RotatingFileHandler('log.txt', maxBytes=10000000, backupCount=5)
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)

    # add the handler to the root logger
    logging.getLogger('').addHandler(handler)

    logging.getLogger('').debug('root logger configuration finished')
    pass

def notify(summary, body='', app_name='', app_icon='', timeout=5000, actions=[], hints=[], replaces_id=0):
    _bus_name = 'org.freedesktop.Notifications'
    _object_path = '/org/freedesktop/Notifications'
    _interface_name = _bus_name

    session_bus = dbus.SessionBus()
    obj = session_bus.get_object(_bus_name, _object_path)
    interface = dbus.Interface(obj, _interface_name)
    interface.Notify(app_name, replaces_id, app_icon, summary, body, actions, hints, timeout)

def my_excepthook(exType, exValue, exTraceback):
    filename, line, dummy, dummy = traceback.extract_tb(exTraceback).pop()
    filename = os.path.basename(filename)
    error = "%s: %s" % (exType.__name__, exValue)

    print "Closed due to an error. This is the full error report:"
    print
    print "".join(traceback.format_exception(exType, exValue, exTraceback))
    sys.exit(1)

#------------------------------------------------------------------------------------------------------------------------------------
class readable_dir(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        prospective_dir=values
        if not os.path.isdir(prospective_dir):
            raise argparse.ArgumentTypeError("{0} is not a valid path".format(prospective_dir))
        if os.access(prospective_dir, os.R_OK):
            setattr(namespace,self.dest,prospective_dir)
        else:
            raise argparse.ArgumentTypeError("{0} is not a readable dir".format(prospective_dir))

#------------------------------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    sys.excepthook = my_excepthook
    initGlobalLogger()

    logger = logging.getLogger('__main__')

    ldir = tempfile.mkdtemp()
    atexit.register(lambda dir=ldir: shutil.rmtree(ldir))

    parser = argparse.ArgumentParser(description='test', fromfile_prefix_chars="@")
    parser.add_argument( dest='dirIn',action=readable_dir, help='input directory: source of unsorted photos')
    parser.add_argument( dest='dirOut',action=readable_dir, help='output directory: destinatoin of sorted photos')

    try:
        args = parser.parse_args()
    except Exception as e:
        logger.error(e)
        sys.exit("aa! errors!")

    # sourceDir = "/mnt/d1/public/!tempFoto/_toProc/"
    # destDir = "/mnt/d1/public/_foto/{YYYY}/{MM}/{DD}/"

    sourceDir = args.dirIn
    destDir = os.path.join(args.dirOut, '{YYYY}/{MM}/{DD}/')

    processorPath      = {'provider': FileSystemItemProvider(sourceDir), 'factory': MediaItemFactory(), 'task': MediaItemTask_Copy(destinationFormat = destDir, fileNameFormat = "{YYYY}{MM}{DD}_{hh}{mm}{ss}.{fileExt}", dryRun = False)}
    #processorPath_Hash = {'provider': FileSystemItemProvider(sourceDir), 'factory': HashedItemFactory(), 'task': HashedItemTask_FindDuplicates(destinationFormat = "/home/ziemek/Temp/duplicates/{YYYY}/{MM}/", fileNameFormat = "{YYYY}{MM}{DD}_{hh}{mm}{ss}.{fileExt}", dryRun = False)}

    processorPath['provider'].process()
    processorPath['factory'].process(processorPath['provider'].fileItems)
    processorPath['task'].process(processorPath['factory'].mediaItems)

    # processorPath_Hash['provider'].process()
    # processorPath_Hash['factory'].process(processorPath_Hash['provider'].fileItems)
    # processorPath_Hash['task'].process(processorPath_Hash['factory'].hashedItems)
