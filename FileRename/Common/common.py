
import logging
import logging.config

#------------------------------------------------------------------------------------------------------------------------------------
# Common
#------------------------------------------------------------------------------------------------------------------------------------
class CommonObject(object):
    def __init__(self):
        object.__init__(self)
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class CommonObjectLogging(CommonObject):
    logger = None
    
    def __init__(self):
        CommonObject.__init__(self)        
        self.logger = logging.getLogger(type(self).__name__)
    
#         # set up default logging (console)
#         logging.basicConfig(level=logging.INFO, format='%(name)-12s: %(levelname)-8s %(message)s', datefmt='%m-%d %H:%MgStarred.py')
#         
#         # set up logging to file
#         handler = logging.handlers.RotatingFileHandler('log.txt', maxBytes=10000000, backupCount=5)
#         handler.setLevel(logging.INFO)
#         formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
#         handler.setFormatter(formatter)

#        self.logger.addHandler(handler)

#------------------------------------------------------------------------------------------------------------------------------------
class CommonItem(CommonObjectLogging):
    def __init__(self):
        CommonObjectLogging.__init__(self)
        pass

#------------------------------------------------------------------------------------------------------------------------------------
class CommonException(CommonObjectLogging, Exception):
    def __init__(self, message = None):
        CommonObjectLogging.__init__(self)
        if(message != None):
            self.logger.error(message)
        Exception.__init__(self, message)

#------------------------------------------------------------------------------------------------------------------------------------
class CommonValueErrorException(CommonObjectLogging, ValueError):
    def __init__(self, message = None):
        CommonObjectLogging.__init__(self)
        if(message != None):
            self.logger.error(message)
        ValueError.__init__(self, message)
