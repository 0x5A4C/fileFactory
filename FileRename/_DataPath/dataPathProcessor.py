
from Common.common import CommonItem, CommonException

#------------------------------------------------------------------------------------------------------------------------------------
# DataPathProcessor
#------------------------------------------------------------------------------------------------------------------------------------
class CommonDataPathProcessor(CommonItem):

    dryRun = True

    def __init__(self):
        CommonItem.__init__(self)

    def process(self, mediaItems):
        raise NotImplementedError("Please Implement this method")

#------------------------------------------------------------------------------------------------------------------------------------
class CommonDataPathProcessorTaskException(CommonException):    
    def __init__(self, message = ""):
        CommonException.__init__(self, message)
