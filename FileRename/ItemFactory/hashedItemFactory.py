from ItemFactory.itemFactory import ItemFactory
from Items.hashedItem import HashedItem

#------------------------------------------------------------------------------------------------------------------------------------
# HashedItemFactory
#------------------------------------------------------------------------------------------------------------------------------------
class CommonHashedItemFactory(ItemFactory): 
    hashedItems = []
       
    def __init__(self):
        ItemFactory.__init__(self)
        pass
    
    def getHashedItem(self, fileSystemItem):
        hashedItem = None
        try:
            hashedItem = HashedItem(fileSystemItem)
        except Exception:
            self.logger.debug("HashedItem not created for file sysytem item {0}".format(fileSystemItem.extension.id))
        return hashedItem
        
    def process(self, fileSystemItems):
        hashedItem = None
        for fileSystemItem in fileSystemItems:
            hashedItem = self.getHashedItem(fileSystemItem)
            if(hashedItem != None):
                self.hashedItems.append(hashedItem)
            else:
                pass
            
        self.logger.info("hashedItems: {0}".format(len(self.hashedItems)))
    
#------------------------------------------------------------------------------------------------------------------------------------
class HashedItemFactory(CommonHashedItemFactory):
    def __init__(self):
        CommonHashedItemFactory.__init__(self)
        pass

    def getHashedItem(self, fileSystemItem):
        return CommonHashedItemFactory.getHashedItem(self, fileSystemItem)

    def process(self, fileSystemItems):
        CommonHashedItemFactory.process(self, fileSystemItems)
