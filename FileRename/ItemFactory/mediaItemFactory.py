from Common.common import CommonItem, CommonException
from ItemFactory.itemFactory import ItemFactory
from Items.mediaItem import MediaItemPicture_Jpg, MediaItemPicture_Jpeg, MediaItemPicture_CanonRaw, MediaItemPicture_Avi, CommonMediaItemException,\
    MediaItemPicture_Thm, MediaItemPicture_Crw, MediaItemPicture_CanonAvi, MediaItemPicture_Mp4, MediaItemPicture_Mov

#------------------------------------------------------------------------------------------------------------------------------------
# MediaItemFactory
#------------------------------------------------------------------------------------------------------------------------------------
class CommonMediaItemFactory(ItemFactory): 
    mediaItems = []
       
    def __init__(self):
        ItemFactory.__init__(self)
        pass
    
    def getMediaItem(self, fileSystemItem):
        raise NotImplementedError("Please Implement this method")
    
    def process(self, fileSystemItems):
        raise NotImplementedError("Please Implement this method")        
    
#------------------------------------------------------------------------------------------------------------------------------------
class MediaItemFactory(CommonMediaItemFactory):
#    MediaItemTypes = [MediaItemPicture_Jpg, MediaItemPicture_Thm, MediaItemPicture_Crw, MediaItemPicture_Avi]
    MediaItemTypes = [MediaItemPicture_Jpg, MediaItemPicture_Jpeg, MediaItemPicture_CanonRaw, MediaItemPicture_CanonAvi, MediaItemPicture_Mp4, MediaItemPicture_Mov]

    def __init__(self):
        CommonMediaItemFactory.__init__(self)
        pass

    def getMediaItem(self, fileSystemItem):
        pictureMediaItem = None
        for mediaItemType in self.MediaItemTypes:
            try:
                pictureMediaItem = mediaItemType(fileSystemItem)
            except CommonMediaItemException:
                self.logger.debug("PictureMediaItem {0} not created for file sysytem item {1}".format(mediaItemType.id, fileSystemItem.extension.id))
                continue

            return pictureMediaItem
        
    def process(self, fileSystemItems):
        for fileSystemItem in fileSystemItems:
            mediaItem = self.getMediaItem(fileSystemItem)
            if(mediaItem != None):
                self.mediaItems.append(mediaItem)
            else:
#                self.logger.info("MediaItem was not created for: {0}".format(fileSystemItem.fullFileName))
                pass
            
        self.logger.info("mediaItems: {0}".format(len(self.mediaItems)))
        
