from Common.common import CommonItem

#------------------------------------------------------------------------------------------------------------------------------------
# MediaItemFactory
#------------------------------------------------------------------------------------------------------------------------------------
class CommonItemFactory(CommonItem): 
    mediaItems = []
       
    def __init__(self):
        CommonItem.__init__(self)
        pass
        
    def process(self, items):
        raise NotImplementedError("Please Implement this method")        
    
#------------------------------------------------------------------------------------------------------------------------------------
class ItemFactory(CommonItemFactory):
    def __init__(self):
        CommonItemFactory.__init__(self)
        pass
    
    def process(self, items):
        raise NotImplementedError("Please Implement this method") 
